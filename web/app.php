<?php

require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Exception\ParseException;

$request = Request::createFromGlobals();

$loader = new Twig_Loader_Filesystem(array(__DIR__, __DIR__.'/twig'));
$twig = new Twig_Environment($loader);
$yaml = new Parser();

include __DIR__.'/../src/routes.php';
$path = $request->getPathInfo();
$file = sprintf('twig/%s.html', $map[$path]['name']);

$partial = $map[$path]['partial'];

try {
    $scripts = $yaml->parse(file_get_contents(__DIR__.'/scripts.yml'));
} catch (ParseException $e) {
    printf("Unable to parse the YAML string: %s", $e->getMessage());
}

if (isset($map[$path]['name'])) {
    ob_start();
    extract($request->query->all(), EXTR_SKIP);
    echo $twig->render($file, array(
        'partial' => $partial,
        'scripts' => $scripts,
    ));
    $response = new Response(ob_get_clean());
} else {
    $response = new Response('Not Found', 404);
}

$response->send();

