$(document).ready(function() {
  $('.sidenav').affix({
    offset: {
      top: 536
    }
  });

  $('.flexslider').flexslider({
    animation: "slide",
    touch: true,
    keyboard: true,
    start: function(slider){
      $('body').removeClass('loading');
    }
  });
});

/* fires modal */
$('.login').on('click', function(e) {
  e.preventDefault();
  $('#login').modal();
});

Modernizr.addTest('webkit', function () {
 return !!navigator.userAgent.match(/webkit/i);
});

$.fn.slideFadeToggle = function(speed, easing, callback) {
  return this.animate({
      opacity: 'toggle',
      height: 'toggle'
  }, speed, easing, callback);
};

svgeezy.init(false, 'png');
//var bbbprotocol = ( ("https:" == document.location.protocol) ? "https://" : "http://" ); document.write(unescape("%3Cscript src='" + bbbprotocol + 'seal-nashville.bbb.org' + unescape('%2Flogo%2Fasurion-insurance-services-2131781.js') + "' type='text/javascript'%3E%3C/script%3E"));
(function(){
	pcc_api.load({
		component: "deductiblemanager", //This tells PCC to return the deductiblemanager component
		carrier: "att",
		container: "phone-drop", //The id of the div where the deductible lookup should appear
		manufacturer_field_label: {
			active: 0
		},
		man_selectval: "Manufacturer",
		model_field_label: {
			active: 0
		},
		mod_selectval: "Model",
		custom_results: '<div class="my_deductible" style="display: none;"><h2>My <span class="bold">Deductible</span> is <span class="blue bold">${{deductible}}</span></h2><div class="phone"><p>{{manufacturer}} {{model}}</p></div><a href="claim_link" class="btn btn-primary" id="claimLink">File a Claim</a><div class="text-center select-device"><a href="/" class="different-device">Select a different device</a></div></div>',
		bootstrap_select: true,
		return_css: 0,
		results_callback: 'deductible_callback',
	});
})();

function deductible_callback(){
	var claimlink = encodeURI("#");
  $('.deductible-callout').hide();
	$('#claimLink').attr('href', claimlink);
	var maincall = '.main-call h2:first, .main-call p:first, #selection_container'
	$(maincall).hide();
	$('.my_deductible').fadeIn();
    $('.my_deductible .different-device').on('click', function(e){
		e.preventDefault();
    $('.deductible-callout').fadeIn();

		$(maincall).fadeIn();
		$('.my_deductible').hide();
	});
}

$(".scroll").click(function (event) {
    event.preventDefault();
    //calculate destination place
    var dest = 0;
    if ($(this.hash).offset().top > $(document).height() - $(window).height()) {
        dest = $(document).height() - $(window).height();
    } else {
        dest = $(this.hash).offset().top;
    }
    //go to destination
    $('html,body').animate({
        scrollTop: dest
    }, 1000, 'swing');
});

(function() {
	var triggerBttn = document.getElementById( 'trigger-overlay' ),
		overlay = document.querySelector( 'div.overlay' ),
		closeBttn = overlay.querySelector( 'a.overlay-trigger-close' ),
		//navBttn = overlay.querySelector( 'a.scroll' );
		transEndEventNames = {
			'WebkitTransition': 'webkitTransitionEnd',
			'MozTransition': 'transitionend',
			'OTransition': 'oTransitionEnd',
			'msTransition': 'MSTransitionEnd',
			'transition': 'transitionend'
		},
		transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
		support = { transitions : Modernizr.csstransitions };

	function toggleOverlay() {
		if( $(overlay).hasClass('open') ) {
			$(overlay).removeClass('open');
			$(overlay).addClass('overlay-close');
			var onEndTransitionFn = function( ev ) {
				if( support.transitions ) {
					if( ev.propertyName !== 'visibility' ) return;
					this.removeEventListener( transEndEventName, onEndTransitionFn );
				}
				$(overlay).removeClass('overlay-close');
				$('body').off('wheel.modal mousewheel.modal');
        //BackgroundCheck.refresh();
			};
			if( support.transitions ) {
				overlay.addEventListener( transEndEventName, onEndTransitionFn );
			}
			else {
				onEndTransitionFn();
			}
		}
		else if( !($(overlay).hasClass('overlay-close')) ) {
			$(overlay).addClass('open');
    	$('body').on('wheel.modal mousewheel.modal', function () {return false;});
		}
    if ( $(window).width() > 700) { 
      $('body').off('wheel.modal mousewheel.modal');
      $(overlay).removeClass('open');
      BackgroundCheck.refresh();
    }
	}
	triggerBttn.addEventListener( 'click', toggleOverlay );
	closeBttn.addEventListener( 'click', toggleOverlay );
	//navBttn.addEventListener( 'click', toggleOverlay );
})();
