1) Configure host entry
----------------------
- Locate your host file (/etc/hosts for Mac)
- Add entry for application (127.0.0.1 ui-att-mdpp.dev)

2) Configure virtual host
----------------------
- Locate the vhosts file (/etc/apache2/httpd.conf)
-- If your file contains a file *vhosts.conf, use that file
- Add a virtual host*

```

    # Be sure to change this to your website path + /web
    <VirtualHost *:80>
        DocumentRoot "/Users/thomas.bennett/Sites/enterprise/ui_att_mdpp/web"
        ServerName ui-att-mdpp.dev
        DirectoryIndex app.php
        ErrorLog "/Users/thomas.bennett/Sites/enterprise/ui_att_mdpp/web/error.log"
        <Directory "/Users/thomas.bennett/Sites/enterprise/ui_att_mdpp/web">
            AllowOverride All
            Options Indexes FollowSymLinks
        </Directory>
    </VirtualHost>
```

3) Restart Apache
----------------------
    $ sudo apachectl restart

4) Download and run Composer to get dependencies
----------------------
    curl -sS https://getcomposer.org/installer | php
or
    https://getcomposer.org/download/

Run composer:
    php composer.phar update

5) Understanding the app
----------------------
- Inside the web directory are various modules that compose each page
- Pages are found under web/twig
- Template wrapper is found under the web directory as "base.twig.html"
- To view a page or a module visit the name of the file (without the underscore):
- Examples:
--> http://ui-att-mdpp.dev/app.php/index
--> http://ui-att-mdpp.dev/app.php/documentation
--> http://ui-att-mdpp.dev/app.php/columns-3

6) Adding content messages
----------------------
The purpose of the HTML files is to provide a consistent layout that can 
be reused as components on multiples pages. In order to do this, we should 
keep the HTML the same, unless absolutely necessary, and only swap out the content.

Inside web/html/index.html for example we see the following:
    {% include partial ? "twig/_featured-hero.html" : "_featured-hero.html" with { 'msg': scripts.page.test } %}

In this example we tell the featured-hero (example.dev/app/featured-hero) to use 
the variable "msg" to use the page.test text found in scripts.yml. The featured-hero 
file will will contain your HTML (that won't change) with the "msg" variable/test.

Example:
    <p>{{ msg }}</p>

Will grab from scripts.yml:
    page:
        test: Your paragraph text.
    
