<?php

/**
 * The key is the name of the page
 * The value is the name of the file
 * Find values under web/
 * SASS include will match the file name
 */

$map = array(

    # Pages
    '/index' => array(
        'name' => 'index',
        'partial' => false,
    ),
    '/protection-plans' => array(
        'name' => 'protection-plans',
        'partial' => false,
    ),
    '/enroll-upgrade' => array(
        'name' => 'enroll-upgrade',
        'partial' => false,
    ),
    '/enhanced-support' => array(
        'name' => 'enhanced-support',
        'partial' => false,
    ),
    '/claims' => array(
        'name' => 'claims',
        'partial' => false,
    ),
    '/contact' => array(
        'name' => 'contact',
        'partial' => false,
    ),
    '/faq' => array(
        'name' => 'faq',
        'partial' => false,
    ),
    '/att-protection-app' => array(
        'name' => 'att-protection-app',
        'partial' => false
    ),

    # HTML Modules / Partials
    '/modal' => array(
        'name' => '_modal',
        'partial' => true,
    ),
    '/header' => array(
        'name' => '_header',
        'partial' => true,
    ),
    '/footer' => array(
        'name' => '_footer',
        'partial' => true,
    ),
    '/buttons-1' => array(
       'name' => '_buttons-1',
        'partial' => true,
    ),
    '/buttons-2' => array(
        'name' => '_buttons-2',
        'partial' => true,
    ),
    '/buttons-3' => array(
        'name' => '_buttons-3',
        'partial' => true,
    ),
    '/buttons-claims' => array(
        'name' => 'buttons-claims',
        'partial' => true,
    ),
    '/columns-two' => array(
        'name' => '_columns-two',
        'partial' => true,
    ),
    '/columns-three' => array(
        'name' => '_columns-three',
        'partial' => true,
    ),
    '/columns-three-2' => array(
        'name' => '_columns-three-2',
        'partial' => true,
    ),
    '/compare' => array(
        'name' => '_compare',
        'partial' => true,
    ),
    '/compare-table' => array(
        'name' => '_compare-table',
        'partial' => true,
    ),
    '/content' => array(
        'name' => '_content',
        'partial' => true,
    ),
    '/content-left' => array(
        'name' => '_content-left',
        'partial' => true,
    ),
    'content-right' => array(
        'name' => '_content-right',
        'partial' => true,
    ),
    '/deductible-lookup' => array(
        'name' => '_deductible-lookup',
        'partial' => true,
    ),
    '/featured-hero' => array(
        'name' => '_featured-hero',
        'partial' => true,
    ),
    '/featured-hero-2' => array(
        'name' => '_featured-hero-2',
        'partial' => true,
    ),
    '/featured-hero-left' => array(
        'name' => '_featured-hero-left',
        'partial' => false,
    ),
    '/featured-hero-right' => array(
        'name' => '_featured-hero-right',
        'partial' => false,
    ),
    '/featured-hero-center' => array(
        'name' => '_featured-hero-center',
        'partial' => false,
    ),
    '/shipping-schedule' => array(
        'name' => '_shipping-schedule',
        'partial' => true,
    ),
    '/steps' => array(
        'name' => '_steps',
        'partial' => true,
    ),
);
